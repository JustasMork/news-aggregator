<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>News agreggator</title>
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="{{ asset('scripts/jquery.js') }}"></script>
        <script src="{{ asset('scripts/bootstrap.min.js') }}"></script>
        <script src="{{ asset('scripts/main.js') }}"></script>


    </head>
    <body>

        @yield('menuTop')
        <div class="container">
            @yield('content')
        </div>
        @include('layouts.footer')

    </body>

</html>
