<nav class="navbar navbar-light top-menu">
    <div class="container">
        <div class="col-md-12">
            <a class="navbar-brand" href="{{route('home')}}">{{ __('News aggregator') }}</a>
            <div class="d-inline float-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('Filters') }}
                    </button>
                    <div class="dropdown-menu">
                        @foreach($categories as $category)
                        <a class="dropdown-item {{ request()->is($category->name) ? 'active' : '' }}" href="{{ route('getCategoryArticles', $category->name) }}">{{$category->name}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
