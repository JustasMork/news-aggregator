@extends('layouts.app')

@section('menuTop')
    @include('layouts.menuTop')
@endsection

@section('content')
    <div class="row">
        @foreach($posts as $post)
        <div class="card post-item" style="margin: 5px">
            <div class="card-header">
                <a href="javascript:void(0)" onclick="openArticleModal({{$post->id}})"><h4 class="card-title">{{$post->title}}</h4></a>
            </div>
            <div class="card-body">
                <p class="card-text">{!! $post->description !!}</p>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <span class="text-muted d-inline">{{$post->date_published}}</span>
                    <a href="{{$post->feedProvider->link}}" target="_blank"><h5 class="d-inline"><span class="badge badge-primary">{{$post->feedProvider->name}}</span></h5></a>
                    <h5 class="d-inline"><span class="badge badge-secondary">{{$post->category->name}}</span></h5>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    {{ $posts->links() }}
@endsection


