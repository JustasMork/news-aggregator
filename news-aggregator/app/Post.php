<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'link', 'description', 'date_published', 'guid', 'category_id', 'feed_provider_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function feedProvider()
    {
        return $this->belongsTo('App\FeedProvider', 'feed_provider_id');
    }
}
