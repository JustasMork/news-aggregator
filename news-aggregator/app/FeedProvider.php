<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedProvider extends Model
{
    protected $fillable = [
      'name', 'link'
    ];
}
