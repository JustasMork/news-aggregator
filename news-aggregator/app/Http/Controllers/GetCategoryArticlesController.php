<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetCategoryArticlesController extends Controller
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Collection
     */
    protected $posts;

    public function __invoke(Request $request, $categoryName) : View
    {
        $this->category = Category::where('name', 'LIKE', $categoryName)->first();

        $this->checkIfCategoryExists();
        $this->getCategoryPosts();

        return view('home')->with('posts', $this->posts)
            ->with('categories', Category::all());
    }

    protected function checkIfCategoryExists() : void
    {
        if(!$this->category)
            throw new NotFoundHttpException('Page not found');
    }

    protected function getCategoryPosts() : void
    {
        $this->posts = Post::where('category_id', '=', $this->category->id)
            ->orderByDesc('date_published')
            ->paginate(15);
    }
}
