<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\UpdateFeeds;
use GuzzleHttp\Client;

class ParseFeed extends Controller
{
    /**
     * @var Client
     */
    protected $client;

    public function __invoke() : void
    {
        UpdateFeeds::dispatch();
    }

}
