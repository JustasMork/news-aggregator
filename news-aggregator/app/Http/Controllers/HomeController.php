<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-03-09
 * Time: 15:33
 */
declare(strict_types=1);


namespace App\Http\Controllers;


use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController
{

    public function __invoke(Request $request) : View
    {
        $posts = Post::orderByDesc('date_published')->paginate(15);
        return view('home')->with('posts', $posts)
            ->with('categories', Category::all());
    }
}
