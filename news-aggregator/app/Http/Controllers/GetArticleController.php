<?php

namespace App\Http\Controllers;

use App\Post;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class GetArticleController extends Controller
{
    /**
     * @var Post
     */
    protected $post;

    public function __invoke($id) : JsonResponse
    {
        $this->post = Post::find($id);

        $this->checkIfPostExists();

        return response()->json([
            'post' => $this->post,
        ], Response::HTTP_OK);
    }

    protected function checkIfPostExists() : void
    {
        if(!$this->post)
            throw new HttpResponseException(response()->json([
                'error' => 'Post not found',
            ])->status(Response::HTTP_NOT_FOUND));
    }
}
