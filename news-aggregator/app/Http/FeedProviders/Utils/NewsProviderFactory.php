<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-03-09
 * Time: 14:20
 */
declare(strict_types=1);


namespace App\Http\FeedProviders;


use App\Http\FeedProviders\Contracts\iFeedProvider;
use Prophecy\Exception\Doubler\MethodNotFoundException;

class NewsProviderFactory
{

    public function getProvider(String $providerName, $providerConfigData) : iFeedProvider
    {
        if(strtolower($providerName) == 'google')
            return new GoogleFeedProvider($providerConfigData['baseUrl'], $providerConfigData['paths']);
        else if(strtolower($providerName) == 'reuters')
            return new ReutersNewsProvider($providerConfigData['baseUrl'], $providerConfigData['paths']);
        else if(strtolower($providerName) == 'cnn')
            return new CnnNewsProvider($providerConfigData['baseUrl'], $providerConfigData['paths']);
        else
            throw new MethodNotFoundException("This news provider is not implemented");
    }

}
