<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-03-09
 * Time: 11:31
 */
declare(strict_types=1);


namespace App\Http\FeedProviders\Contracts;


interface iFeedProvider
{
    /**
     * @return void
     */
    public function fetch();

}
