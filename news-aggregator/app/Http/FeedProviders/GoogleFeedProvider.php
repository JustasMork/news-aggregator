<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-03-09
 * Time: 11:28
 */
declare(strict_types=1);


namespace App\Http\FeedProviders;

use App\Post;

class GoogleFeedProvider extends FeedProvider
{

    protected function saveElement(\SimpleXMLElement $item) : void
    {
        $this->modifyElement($item);
        Post::updateOrCreate(
            ['guid' => $item->guid],
            [
                'category_id' => $this->category->id,
                'feed_provider_id' => $this->feedProvider->id,
                'title' => $item->title,
                'link' => $item->link,
                'description' => $item->description,
                'date_published' => new \DateTime((String)$item->pubDate),
            ]
        );
    }

    protected function modifyElement($item) : \SimpleXMLElement
    {
        if(strlen((string)$item->guid) > 100)
            $item->guid = substr((string)$item->guid, strlen((string)$item->guid) - 100, 100);

        return $item;
    }

    protected function getFeedProviderName() : String
    {
        return "Google";
    }
}
