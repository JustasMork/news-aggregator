<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 2019-03-09
 * Time: 11:26
 */
declare(strict_types=1);

namespace App\Http\FeedProviders;

use App\Category;
use App\Http\FeedProviders\Contracts\iFeedProvider;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

abstract class FeedProvider implements iFeedProvider
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var \App\FeedProvider
     */
    protected $feedProvider;

    /**
     * @var String
     */
    protected $baseUrl;

    /**
     * @var String[]
     */
    protected $pathsArray;

    public function __construct(string $baseUrl, $pathsArray)
    {
        $this->baseUrl = $baseUrl;
        $this->pathsArray = $pathsArray;
        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'verify' => storage_path(config('feed.ver_cert'))
        ]);
    }

    public function fetch() : void
    {
        foreach ($this->pathsArray as $category => $path)
        {
            $this->saveFeedProviderIfNew($path);
            $this->fetchCategory($category);
            $this->fetchElementsFromPath($path);
        }
    }

    protected function saveFeedProviderIfNew($path) : void
    {
        $link = $this->baseUrl.'/'.$path;
        $feedProvider = \App\FeedProvider::where('link', '=', $link)->first();
        if(!$feedProvider)
        {
            $feedProvider = new \App\FeedProvider();
            $feedProvider->name = $this->getFeedProviderName();
            $feedProvider->link = $link;
            $feedProvider->save();
        }
        $this->feedProvider = $feedProvider;
    }

    protected function fetchCategory($categoryName) : void
    {
        if(is_int($categoryName))
            $categoryName = "Uncategorized";

        $category = Category::where('name', '=', $categoryName)->first();
        if($category === null)
        {
            $category = new Category();
            $category->name = $categoryName;
            $category->save();
        }
        $this->category = $category;
    }

    protected function fetchElementsFromPath(String $path) : void
    {
        $response = $this->client->get($path);

        if($response->getStatusCode() == Response::HTTP_OK)
        {
            $xmlItems = $this->parseXMLContent($response->getBody()->getContents());
            foreach ($xmlItems as $item)
                $this->saveElement($item);
        }
    }

    /**
     * @return \SimpleXMLElement[]
     */
    protected function parseXMLContent(String $content)
    {
        $items = [];
        $contents = new \SimpleXMLElement($content);
        foreach ($contents->channel->item as $item)
            $items[] = $item;

        return $items;
    }

    /**
     * @return void
     */
    protected abstract function saveElement(\SimpleXMLElement $item);

    /**
     * @return String
     */
    protected abstract function getFeedProviderName();
}
