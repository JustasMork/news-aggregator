
modal = {
    id: '#modal',
    headerData: '',
    bodyData: '',
    callback: null,
    callbackParams: [],

    setHeader: function(data){
      headerData = data;
      return this;
    },

    setBody: function(data){
        bodyData = data;
        return this;
    },

    open: function () {
        $(this.id + ' .modal-body').html(bodyData);
        $(this.id + ' .modal-header h3').html(headerData);
        $(this.id).modal('show');
    },

    setConfirmButtonCallback: function(callback){
        this.callback = callback;
        if(arguments.length > 1){
            var args = arguments;
            console.log(arguments);

            this.callbackParams = args;
        }
        return this;

    },

    triggerConfirmCallback: function() {
        if (this.callbackParams.length == 1)
            this.callback();
        else if (this.callbackParams.length == 2)
            this.callback(this.callbackParams[1]);
        else if (this.callbackParams.length == 3)
            this.callback(this.callbackParams[1], this.callbackParams[2]);
    },

    close: function () {
        $(this.id).modal('hide');
    },
};

function openArticleModal(articleId) {
    console.log('Clicked');
    $.get('/news/'+articleId, function (response) {
        modal.setHeader(response.post.title)
            .setBody(response.post.description)
            .setConfirmButtonCallback()
            .setConfirmButtonCallback(openPageOnNewTab, response.post.link)
            .open();
    })
}

function openPageOnNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}
