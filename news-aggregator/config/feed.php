<?php

return [
    'ver_cert' => 'app/cacert.pem',

    'providers' => [
        'google' => [
            'baseUrl' => 'https://news.google.com',
            'paths' => ['rss?hl=lt&gl=LT&ceid=LT:lt']
        ],
        'reuters' => [
            'baseUrl' => 'http://feeds.reuters.com',
            'paths' => [
                'Business' => 'reuters/businessNews',
                'World' => 'Reuters/worldNews',
                'Sports' => 'reuters/sportsNews',
                'Politics' => 'Reuters/PoliticsNews',
            ]
        ],
        'cnn' => [
            'baseUrl' => 'http://rss.cnn.com',
            'paths' => [
                'Business' => 'rss/edition_world.rss',
                'World' => 'rss/edition_world.rss',
                'Sports' => 'rss/edition_sport.rss',
                'Technology' => 'rss/edition_technology.rss',
                'Travel' => 'rss/edition_travel.rss',
            ]
        ]
    ]
];
