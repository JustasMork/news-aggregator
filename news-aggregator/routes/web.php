<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomeController::class)->name('home');

Route::get('/cron/parseFeed', ParseFeed::class)->name('feedParser');

Route::get('/news/{id}', GetArticleController::class)->name('getArticle');

Route::get('/{categoryName}', GetCategoryArticlesController::class)->name('getCategoryArticles');
